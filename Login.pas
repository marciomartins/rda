unit Login;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Imaging.jpeg;

type
  TfrLogin = class(TForm)
    Panel2: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    leUsuario: TLabeledEdit;
    leSenha: TLabeledEdit;
    PanelBottom: TPanel;
    btOk: TButton;
    btCancel: TButton;
    btConexao: TButton;
    procedure btOkClick(Sender: TObject);
    procedure btCancelClick(Sender: TObject);
    procedure btConexaoClick(Sender: TObject);
  private
    function LoginValido(const Nome, Senha: string): Boolean;
    { Private declarations }
  public
    { Public declarations }

  end;

const
  UsuarioMaster : string = 'Datafocus';
  SenhaMaster : string = '8782AD648AF967DD4DC9';


var
  frLogin: TfrLogin;

implementation

{$R *.dfm}

uses Settings, ConnectionSettings, UsuarioController, Usuario, Security;

procedure TfrLogin.btCancelClick(Sender: TObject);
begin
  Close;
  ModalResult := mrCancel;
end;

procedure TfrLogin.btConexaoClick(Sender: TObject);
var
  Form: TfrConexao;
begin
  Form := TfrConexao.Create(nil);
  try
    Form.ShowModal;
  finally
    Form.Free;
  end;
end;

function TfrLogin.LoginValido(const Nome, Senha: string):Boolean;
var
  UsuarioController: TUsuarioController;
  Usuario: TUsuario;
begin
  Result := False;
  UsuarioController := TUsuarioController.Create;
  Usuario := UsuarioController.FindByName(Nome);
  if Usuario <> nil then
  begin
    Result := Usuario.Nome.Equals(Nome) and Crypt(caDecrypt,Usuario.Senha).Equals(Senha);
    if Result then
    begin
      TSettings.Instance.Usuario.Nome := Usuario.Nome;
      TSettings.Instance.Usuario.Id := Usuario.Id;
    end
    else
      ShowMessage('Usu�rio ou senha inv�lido(s)!');
  end
  else
    ShowMessage('Usu�rio ou senha inv�lido(s)!');
end;

procedure TfrLogin.btOkClick(Sender: TObject);
begin
  if SameText(leUsuario.Text,UsuarioMaster) and SameText(leSenha.Text,Crypt(caDecrypt,SenhaMaster))then
  begin
    TSettings.Instance.Usuario.Nome := UsuarioMaster;
    TSettings.Instance.Usuario.Id := -1;
    Close;
    ModalResult := mrOk;
  end
  else if LoginValido(leUsuario.Text, leSenha.Text) then
  begin
    Close;
    ModalResult := mrOk;
  end;
end;

end.
