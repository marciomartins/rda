unit ListarEmpresas;

interface

uses
  Generics.Collections, Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Grids, Vcl.ExtCtrls, BaseForm, Usuario, EmpresaController;

type
  TfrListarEmpresas = class(TForm)
    PanelTop: TPanel;
    MainPanel: TPanel;
    Grid: TStringGrid;
    PanelBottom: TPanel;
    btNew: TButton;
    btExit: TButton;
    btEdit: TButton;
    btDelete: TButton;
    procedure btEditClick(Sender: TObject);
    procedure btDeleteClick(Sender: TObject);
    procedure btNewClick(Sender: TObject);
    procedure btExitClick(Sender: TObject);
    procedure GridClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FController: TEmpresaController;
    function GetSelectedEmpresa: TEmpresa;
    procedure LoadEmpresas;
  public
    { Public declarations }
  end;

var
  frListarEmpresas: TfrListarEmpresas;

implementation

uses
  EditarEmpresa;

{$R *.dfm}

procedure TfrListarEmpresas.btDeleteClick(Sender: TObject);
var
  Empresa: TEmpresa;
  Msg: string;
begin
  Empresa := GetSelectedEmpresa;

  Msg := 'Tem certeza que voc� deseja excluir a Empresa "' + Empresa.Nome + '"?';

  if MessageDlg(Msg, mtWarning, mbYesNo, 0) = mrYes then
  begin
    FController.DeleteEmpresa(Empresa);
    LoadEmpresas;
  end;
end;

procedure TfrListarEmpresas.btEditClick(Sender: TObject);
var
  Empresa: TEmpresa;
  frEditarEmpresa: TfrEditarEmpresa;
begin
  Empresa := GetSelectedEmpresa;

  frEditarEmpresa := TfrEditarEmpresa.Create(Self);
  try
    frEditarEmpresa.SetEmpresa(Empresa.Id);
    frEditarEmpresa.ShowModal;

    if frEditarEmpresa.ModalResult = mrOk then
      LoadEmpresas;
  finally
    frEditarEmpresa.Free;
  end;
end;

procedure TfrListarEmpresas.btExitClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrListarEmpresas.btNewClick(Sender: TObject);
var
  frEditarEmpresa: TfrEditarEmpresa;
begin
  frEditarEmpresa := TfrEditarEmpresa.Create(Self);
  try
    frEditarEmpresa.ShowModal;

    if frEditarEmpresa.ModalResult = mrOk then
      LoadEmpresas;
  finally
    frEditarEmpresa.Free;
  end;
end;

procedure TfrListarEmpresas.FormCreate(Sender: TObject);
begin
  FController := TEmpresaController.Create;
  LoadEmpresas;
end;

procedure TfrListarEmpresas.FormDestroy(Sender: TObject);
begin
  FController.Free;
end;

function TfrListarEmpresas.GetSelectedEmpresa: TEmpresa;
begin
  Result := TEmpresa(Grid.Cols[0].Objects[Grid.Row]);
end;

procedure TfrListarEmpresas.GridClick(Sender: TObject);
begin
  btEditClick(Sender);
end;

procedure TfrListarEmpresas.LoadEmpresas;
var
  Empresas: TList<TEmpresa>;
  Empresa: TEmpresa;
  I, J: Integer;
begin
  for I := 0 to Grid.RowCount - 1 do
    for J := 0 to Grid.ColCount - 1 do
      Grid.Cells[J, I] := '';
  Grid.RowCount := 2;

  Grid.Cells[0, 0] := 'Nome';

  Empresas := FController.GetAllEmpresas;
  try
    if Empresas.Count > 0 then
    begin
      Grid.RowCount := 1 + Empresas.Count;

      for I := 0 to Empresas.Count - 1 do
      begin
        Empresa := Empresas[I];
        Grid.Cols[0].Objects[I + 1] := Empresa;
        Grid.Cells[0, I + 1] := Empresa.Nome;
      end;
    end;
  finally
    Empresas.Free;
  end;
end;

end.
