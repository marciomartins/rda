object frEditarMaquina: TfrEditarMaquina
  Left = 0
  Top = 0
  Caption = 'Cadastro de M'#225'quina'
  ClientHeight = 362
  ClientWidth = 406
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelTop: TPanel
    Left = 0
    Top = 0
    Width = 406
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    Caption = 'M'#225'quina'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
  end
  object MainPanel: TPanel
    Left = 0
    Top = 41
    Width = 406
    Height = 280
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitHeight = 303
    DesignSize = (
      406
      280)
    object lbDescricao: TLabel
      Left = 16
      Top = 13
      Width = 50
      Height = 13
      Caption = 'Descri'#231#227'o:'
    end
    object lbCliente: TLabel
      Left = 16
      Top = 180
      Width = 37
      Height = 13
      Caption = 'Cliente:'
    end
    object btNovoCliente: TSpeedButton
      Left = 371
      Top = 177
      Width = 23
      Height = 21
      Anchors = [akTop, akRight]
      Caption = '+'
      OnClick = btNovoClienteClick
      ExplicitLeft = 364
    end
    object lbEndereco: TLabel
      Left = 16
      Top = 46
      Width = 49
      Height = 13
      Caption = 'Endere'#231'o:'
    end
    object lbPorta: TLabel
      Left = 16
      Top = 78
      Width = 30
      Height = 13
      Caption = 'Porta:'
    end
    object lbUsuario: TLabel
      Left = 16
      Top = 112
      Width = 40
      Height = 13
      Caption = 'Usu'#225'rio:'
    end
    object lbSenha: TLabel
      Left = 16
      Top = 146
      Width = 34
      Height = 13
      Caption = 'Senha:'
    end
    object Label1: TLabel
      Left = 16
      Top = 216
      Width = 45
      Height = 13
      Caption = 'Empresa:'
    end
    object btNovoEmpresa: TSpeedButton
      Left = 371
      Top = 213
      Width = 23
      Height = 21
      Anchors = [akTop, akRight]
      Caption = '+'
      OnClick = btNovoEmpresaClick
    end
    object edDescricao: TEdit
      Left = 116
      Top = 10
      Width = 278
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 0
    end
    object cbCliente: TComboBox
      Left = 116
      Top = 177
      Width = 248
      Height = 21
      Style = csDropDownList
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 5
    end
    object edEndereco: TEdit
      Left = 116
      Top = 43
      Width = 278
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 1
    end
    object edPorta: TEdit
      Left = 117
      Top = 75
      Width = 278
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 2
    end
    object edUsuario: TEdit
      Left = 116
      Top = 109
      Width = 278
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 3
    end
    object edSenha: TEdit
      Left = 116
      Top = 143
      Width = 278
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      PasswordChar = '*'
      TabOrder = 4
    end
    object cbContinuarIncluindo: TCheckBox
      Left = 16
      Top = 256
      Width = 113
      Height = 17
      Caption = 'Continuar Incluindo'
      Checked = True
      State = cbChecked
      TabOrder = 7
    end
    object cbEmpresa: TComboBox
      Left = 116
      Top = 213
      Width = 248
      Height = 21
      Style = csDropDownList
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 6
    end
  end
  object PanelBottom: TPanel
    Left = 0
    Top = 321
    Width = 406
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitTop = 344
    DesignSize = (
      406
      41)
    object btSave: TButton
      Left = 212
      Top = 7
      Width = 90
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Salvar'
      Default = True
      TabOrder = 0
      OnClick = btSaveClick
    end
    object btCancel: TButton
      Left = 308
      Top = 7
      Width = 90
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Cancelar'
      ModalResult = 2
      TabOrder = 1
      OnClick = btCancelClick
    end
  end
end
