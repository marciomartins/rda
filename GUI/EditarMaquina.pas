unit EditarMaquina;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls, MaquinaController, BaseForm,
  Aurelius.Engine.ObjectManager, Usuario, EmpresaController;

type
  TfrEditarMaquina = class(TBaseForm)
    PanelTop: TPanel;
    MainPanel: TPanel;
    lbDescricao: TLabel;
    lbCliente: TLabel;
    btNovoCliente: TSpeedButton;
    edDescricao: TEdit;
    cbCliente: TComboBox;
    PanelBottom: TPanel;
    btSave: TButton;
    btCancel: TButton;
    lbEndereco: TLabel;
    edEndereco: TEdit;
    lbPorta: TLabel;
    edPorta: TEdit;
    lbUsuario: TLabel;
    edUsuario: TEdit;
    lbSenha: TLabel;
    edSenha: TEdit;
    cbContinuarIncluindo: TCheckBox;
    Label1: TLabel;
    cbEmpresa: TComboBox;
    btNovoEmpresa: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure btSaveClick(Sender: TObject);
    procedure btNovoClienteClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btCancelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btNovoEmpresaClick(Sender: TObject);
  private
    { Private declarations }
    FController: TMaquinaController;
  public
    { Public declarations }
    procedure LoadClientes;
    procedure LoadEmpresas;
    procedure SetMaquina(MaquinaId: Variant);
  end;

var
  frEditarMaquina: TfrEditarMaquina;

implementation

uses EditarCliente, DBConnection, Generics.Collections, Security, EditarEmpresa;

{$R *.dfm}

procedure TfrEditarMaquina.btCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrEditarMaquina.btNovoClienteClick(Sender: TObject);
var
  frEditarCliente: TfrEditarCliente;
begin
  frEditarCliente := TfrEditarCliente.Create(Self);
  try
    frEditarCliente.ShowModal;
    if frEditarCliente.ModalResult = mrOk then
    begin
      LoadClientes;
      cbCliente.ItemIndex := cbCliente.Items.Count - 1;
    end;
  finally
    frEditarCliente.Free;
  end;
end;

procedure TfrEditarMaquina.btNovoEmpresaClick(Sender: TObject);
var
  frEditarEmpresa: TfrEditarEmpresa;
begin
  frEditarEmpresa := TfrEditarEmpresa.Create(Self);
  try
    frEditarEmpresa.ShowModal;
    if frEditarEmpresa.ModalResult = mrOk then
    begin
      LoadEmpresas;
      cbEmpresa.ItemIndex := cbEmpresa.Items.Count - 1;
    end;
  finally
    frEditarEmpresa.Free;
  end;
end;

procedure TfrEditarMaquina.btSaveClick(Sender: TObject);
var
  Maquina: TMaquina;
begin
  Maquina := FController.Maquina;
  Maquina.Descricao := edDescricao.Text;
  Maquina.Endereco := edEndereco.Text;
  if string(edPorta.Text).IsEmpty then
    Maquina.Porta := 3389
  else
    Maquina.Porta := string(edPorta.Text).ToInteger;
  Maquina.Usuario := edUsuario.Text;
  Maquina.Senha := Crypt(caCrypt,edSenha.Text);

  if cbCliente.ItemIndex >= 0 then
    Maquina.Cliente := TCliente(cbCliente.Items.Objects[cbCliente.ItemIndex]);

  if cbEmpresa.ItemIndex >= 0 then
    Maquina.Empresa := TEmpresa(cbEmpresa.Items.Objects[cbEmpresa.ItemIndex]);

  FController.Save(Maquina);
  if cbContinuarIncluindo.Checked then
  begin
    edDescricao.Text := '';
    edEndereco.Text := '';
    edPorta.Text := '';
    edUsuario.Text := '';
    edUsuario.Text := '';
    edSenha.Text := '';
    cbCliente.ItemIndex := -1;
    edDescricao.SetFocus;
  end
  else
    ModalResult := mrOk;
end;

procedure TfrEditarMaquina.FormCreate(Sender: TObject);
begin
  FController := TMaquinaController.Create;
  LoadClientes;
  LoadEmpresas;
end;

procedure TfrEditarMaquina.FormDestroy(Sender: TObject);
begin
  FController.Free;
end;

procedure TfrEditarMaquina.FormShow(Sender: TObject);
begin
  edDescricao.SetFocus;
end;

procedure TfrEditarMaquina.LoadClientes;
var
  Clientes: TList<TCliente>;
  C: TCliente;
begin
  cbCliente.Items.Clear;
  Clientes := FController.GetClientes;
  try
    for C in Clientes do
      cbCliente.AddItem(C.Nome, C);
  finally
    Clientes.Free;
  end;
end;

procedure TfrEditarMaquina.LoadEmpresas;
var
  Empresas: TList<TEmpresa>;
  E: TEmpresa;
begin
  cbEmpresa.Items.Clear;
  Empresas := FController.GetEmpresas;
  try
    for E in Empresas do
      cbEmpresa.AddItem(E.Nome, E);
  finally
    Empresas.Free;
  end;
end;

procedure TfrEditarMaquina.SetMaquina(MaquinaId: Variant);
var
  Maquina: TMaquina;

begin
  FController.Load(MaquinaId);
  Maquina := FController.Maquina;

  edDescricao.Text := Maquina.Descricao;
  edEndereco.Text := Maquina.Endereco;
  edPorta.Text := intToStr(Maquina.Porta);
  edUsuario.Text := Maquina.Usuario;
  edSenha.Text := Crypt(caDecrypt, Maquina.Senha);
  cbCliente.ItemIndex := cbCliente.Items.IndexOf(Maquina.Cliente.Nome);
  cbEmpresa.ItemIndex := cbEmpresa.Items.IndexOf(Maquina.Empresa.Nome);
end;


end.
