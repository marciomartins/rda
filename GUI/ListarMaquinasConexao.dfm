object frmListarMaquinasConexao: TfrmListarMaquinasConexao
  Left = 0
  Top = 0
  Caption = 'M'#225'quinas'
  ClientHeight = 300
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PanelBottom: TPanel
    Left = 0
    Top = 259
    Width = 635
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      635
      41)
    object btExit: TButton
      Left = 537
      Top = 7
      Width = 90
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Fechar'
      ModalResult = 2
      TabOrder = 1
      OnClick = btExitClick
    end
    object btConectar: TButton
      Left = 433
      Top = 6
      Width = 90
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Conectar'
      Default = True
      TabOrder = 0
      OnClick = btConectarClick
    end
  end
  object PanelTop: TPanel
    Left = 0
    Top = 0
    Width = 635
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    Caption = 'M'#225'quinas'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
  end
  object MainPanel: TPanel
    Left = 0
    Top = 41
    Width = 635
    Height = 218
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 8
    TabOrder = 2
    object dbgMain: TDBGrid
      Left = 8
      Top = 8
      Width = 619
      Height = 202
      Align = alClient
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
    end
  end
end
