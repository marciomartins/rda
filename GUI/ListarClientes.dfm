object frListarClientes: TfrListarClientes
  Left = 0
  Top = 0
  Caption = 'Consulta de Clientes'
  ClientHeight = 310
  ClientWidth = 747
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object PanelTop: TPanel
    Left = 0
    Top = 0
    Width = 747
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    Caption = 'Clientes'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitWidth = 392
  end
  object MainPanel: TPanel
    Left = 0
    Top = 41
    Width = 747
    Height = 228
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 8
    TabOrder = 1
    ExplicitWidth = 392
    object Splitter1: TSplitter
      Left = 416
      Top = 8
      Width = 8
      Height = 212
      Align = alRight
      ExplicitLeft = 417
    end
    object dgMain: TDBGrid
      Left = 8
      Top = 8
      Width = 408
      Height = 212
      Align = alClient
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
    end
    object dgMaquinas: TDBGrid
      Left = 424
      Top = 8
      Width = 315
      Height = 212
      Align = alRight
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
    end
  end
  object PanelBottom: TPanel
    Left = 0
    Top = 269
    Width = 747
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      747
      41)
    object btNew: TButton
      Left = 553
      Top = 7
      Width = 90
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Novo'
      TabOrder = 2
      OnClick = btNewClick
      ExplicitLeft = 198
    end
    object btExit: TButton
      Left = 649
      Top = 7
      Width = 90
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Fechar'
      ModalResult = 2
      TabOrder = 3
      OnClick = btExitClick
      ExplicitLeft = 294
    end
    object btEdit: TButton
      Left = 361
      Top = 7
      Width = 90
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Editar'
      Default = True
      TabOrder = 0
      OnClick = btEditClick
      ExplicitLeft = 6
    end
    object btDelete: TButton
      Left = 457
      Top = 7
      Width = 90
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Deletar'
      TabOrder = 1
      OnClick = btDeleteClick
      ExplicitLeft = 102
    end
  end
end
