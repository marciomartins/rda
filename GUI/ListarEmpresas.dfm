object frListarEmpresas: TfrListarEmpresas
  Left = 0
  Top = 0
  Caption = 'Consulta de Empresas'
  ClientHeight = 236
  ClientWidth = 394
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object PanelTop: TPanel
    Left = 0
    Top = 0
    Width = 394
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    Caption = 'Empresas'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitWidth = 384
  end
  object MainPanel: TPanel
    Left = 0
    Top = 41
    Width = 394
    Height = 154
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 384
    ExplicitHeight = 131
    DesignSize = (
      394
      154)
    object Grid: TStringGrid
      Left = 7
      Top = 8
      Width = 379
      Height = 142
      Anchors = [akLeft, akTop, akRight, akBottom]
      ColCount = 1
      DefaultRowHeight = 20
      FixedCols = 0
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRowSelect]
      ScrollBars = ssVertical
      TabOrder = 0
      OnClick = GridClick
      ExplicitWidth = 380
      ExplicitHeight = 118
      ColWidths = (
        372)
    end
  end
  object PanelBottom: TPanel
    Left = 0
    Top = 195
    Width = 394
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitTop = 172
    ExplicitWidth = 384
    DesignSize = (
      394
      41)
    object btNew: TButton
      Left = 200
      Top = 7
      Width = 90
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Novo'
      TabOrder = 2
      OnClick = btNewClick
      ExplicitLeft = 201
    end
    object btExit: TButton
      Left = 296
      Top = 7
      Width = 90
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Fechar'
      ModalResult = 2
      TabOrder = 3
      OnClick = btExitClick
      ExplicitLeft = 297
    end
    object btEdit: TButton
      Left = 8
      Top = 7
      Width = 90
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Editar'
      Default = True
      TabOrder = 0
      OnClick = btEditClick
      ExplicitLeft = 9
    end
    object btDelete: TButton
      Left = 104
      Top = 7
      Width = 90
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Deletar'
      TabOrder = 1
      OnClick = btDeleteClick
      ExplicitLeft = 105
    end
  end
end
