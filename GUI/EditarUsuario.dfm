object frEditarUsuario: TfrEditarUsuario
  Left = 0
  Top = 0
  Caption = 'Cadastro de Usu'#225'rio'
  ClientHeight = 335
  ClientWidth = 456
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelTop: TPanel
    Left = 0
    Top = 0
    Width = 456
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    Caption = 'Usu'#225'rio'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
  end
  object MainPanel: TPanel
    Left = 0
    Top = 41
    Width = 456
    Height = 253
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      456
      253)
    object lbName: TLabel
      Left = 16
      Top = 13
      Width = 31
      Height = 13
      Caption = 'Nome:'
    end
    object Label1: TLabel
      Left = 16
      Top = 44
      Width = 34
      Height = 13
      Caption = 'Senha:'
    end
    object lbEmpresas: TLabel
      Left = 16
      Top = 74
      Width = 50
      Height = 13
      Caption = 'Empresas:'
    end
    object edNome: TEdit
      Left = 64
      Top = 10
      Width = 380
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 0
    end
    object edSenha: TEdit
      Left = 64
      Top = 41
      Width = 380
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      PasswordChar = '*'
      TabOrder = 1
    end
    object clbEmpresas: TCheckListBox
      Left = 16
      Top = 93
      Width = 417
      Height = 124
      ItemHeight = 13
      TabOrder = 2
    end
    object cbContinuarIncluindo: TCheckBox
      Left = 16
      Top = 230
      Width = 113
      Height = 17
      Caption = 'Continuar Incluindo'
      Checked = True
      State = cbChecked
      TabOrder = 3
    end
  end
  object PanelBottom: TPanel
    Left = 0
    Top = 294
    Width = 456
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      456
      41)
    object btSave: TButton
      Left = 258
      Top = 7
      Width = 90
      Height = 28
      Anchors = [akTop, akRight]
      Caption = '&Salvar'
      Default = True
      TabOrder = 0
      OnClick = btSaveClick
    end
    object btCancel: TButton
      Left = 354
      Top = 7
      Width = 90
      Height = 28
      Anchors = [akTop, akRight]
      Caption = '&Cancelar'
      TabOrder = 1
      OnClick = btCancelClick
    end
  end
end
