unit ListarUsuarios;

interface

uses
  Generics.Collections, Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseForm, Usuario, UsuarioController,
  Vcl.StdCtrls, Vcl.Grids, Vcl.ExtCtrls;

type
  TfrListarUsuarios = class(TBaseForm)
    PanelTop: TPanel;
    MainPanel: TPanel;
    Grid: TStringGrid;
    PanelBottom: TPanel;
    btNew: TButton;
    btExit: TButton;
    btEdit: TButton;
    btDelete: TButton;
    procedure btEditClick(Sender: TObject);
    procedure btDeleteClick(Sender: TObject);
    procedure btNewClick(Sender: TObject);
    procedure btExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure GridDblClick(Sender: TObject);
  private
    { Private declarations }
    FController: TUsuarioController;
    function GetSelectedUsuario: TUsuario;
    procedure LoadUsuarios;
  public
    { Public declarations }
  end;

var
  frListarUsuarios: TfrListarUsuarios;

implementation

uses
  EditarUsuario;

{$R *.dfm}

procedure TfrListarUsuarios.btDeleteClick(Sender: TObject);
var
  Usuario: TUsuario;
  Msg: string;
begin
  Usuario := GetSelectedUsuario;

  Msg := 'Tem certeza que voc� deseja excluir o Usu�rio "' + Usuario.Nome + '"?';

  if MessageDlg(Msg, mtWarning, mbYesNo, 0) = mrYes then
  begin
    FController.DeleteUsuario(Usuario);
    LoadUsuarios;
  end;
end;

procedure TfrListarUsuarios.btEditClick(Sender: TObject);
var
  Usuario: TUsuario;
  frEditarUsuario: TfrEditarUsuario;
begin
  Usuario := GetSelectedUsuario;

  frEditarUsuario := TfrEditarUsuario.Create(Self);
  try
    frEditarUsuario.SetUsuario(Usuario.Id);
    frEditarUsuario.ShowModal;

    if frEditarUsuario.ModalResult = mrOk then
      LoadUsuarios;
  finally
    frEditarUsuario.Free;
  end;
end;

procedure TfrListarUsuarios.btExitClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrListarUsuarios.btNewClick(Sender: TObject);
var
  frEditarUsuario: TfrEditarUsuario;
begin
  frEditarUsuario := TfrEditarUsuario.Create(Self);
  try
    frEditarUsuario.ShowModal;

    if frEditarUsuario.ModalResult = mrOk then
      LoadUsuarios;
  finally
    frEditarUsuario.Free;
  end;
end;

procedure TfrListarUsuarios.FormCreate(Sender: TObject);
begin
  FController := TUsuarioController.Create;
  LoadUsuarios;
end;

procedure TfrListarUsuarios.FormDestroy(Sender: TObject);
begin
  FController.Free;
end;

function TfrListarUsuarios.GetSelectedUsuario: TUsuario;
begin
  Result := TUsuario(Grid.Cols[0].Objects[Grid.Row]);
end;

procedure TfrListarUsuarios.GridDblClick(Sender: TObject);
begin
  btEditClick(Sender);
end;

procedure TfrListarUsuarios.LoadUsuarios;
var
  Usuarios: TList<TUsuario>;
  Usuario: TUsuario;
  I, J: Integer;
begin
  for I := 0 to Grid.RowCount - 1 do
    for J := 0 to Grid.ColCount - 1 do
      Grid.Cells[J, I] := '';
  Grid.RowCount := 2;

  Grid.Cells[0, 0] := 'Nome';
  Grid.Cells[1, 0] := 'Senha';

  Usuarios := FController.GetAllUsuarios;
  try
    if Usuarios.Count > 0 then
    begin
      Grid.RowCount := 1 + Usuarios.Count;

      for I := 0 to Usuarios.Count - 1 do
      begin
        Usuario := Usuarios[I];
        Grid.Cols[0].Objects[I + 1] := Usuario;
        Grid.Cells[0, I + 1] := Usuario.Nome;
        Grid.Cells[1, I + 1] := Usuario.Senha;
      end;
    end;
  finally
    Usuarios.Free;
  end;
end;

end.
