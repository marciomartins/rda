unit EditarEmpresa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, BaseForm, Usuario, EmpresaController;

type
  TfrEditarEmpresa = class(TBaseForm)
    PanelTop: TPanel;
    PanelBottom: TPanel;
    btSave: TButton;
    btCancel: TButton;
    MainPanel: TPanel;
    lbNome: TLabel;
    edNome: TEdit;
    cbContinuarIncluindo: TCheckBox;
    procedure btSaveClick(Sender: TObject);
    procedure btCancelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FController: TEmpresaController;
  public
    { Public declarations }
    procedure SetEmpresa(EmpresaId: Variant);
  end;

var
  frEditarEmpresa: TfrEditarEmpresa;

implementation

{$R *.dfm}

procedure TfrEditarEmpresa.btCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrEditarEmpresa.btSaveClick(Sender: TObject);
var
  Empresa: TEmpresa;
begin
  Empresa := FController.Empresa;
  Empresa.Nome := edNome.Text;
  FController.Save(Empresa);
  if cbContinuarIncluindo.Checked then
  begin
    edNome.Text := '';
    edNome.SetFocus;
  end
  else
    ModalResult := mrOk;
end;

procedure TfrEditarEmpresa.FormCreate(Sender: TObject);
begin
  FController := TEmpresaController.Create;
end;

procedure TfrEditarEmpresa.FormDestroy(Sender: TObject);
begin
  FController.Free;
end;

procedure TfrEditarEmpresa.FormShow(Sender: TObject);
begin
  edNome.SetFocus;
end;

procedure TfrEditarEmpresa.SetEmpresa(EmpresaId: Variant);
var
  Empresa: TEmpresa;
begin
  FController.Load(EmpresaId);
  Empresa := FController.Empresa;

  edNome.Text := Empresa.Nome;
end;

end.
