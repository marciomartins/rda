unit ListarMaquinas;

interface

uses
  Generics.Collections, Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Grids, Vcl.ExtCtrls, BaseForm, Usuario, MaquinaController;

type
  TfrListarMaquinas = class(TForm)
    PanelTop: TPanel;
    MainPanel: TPanel;
    Grid: TStringGrid;
    PanelBottom: TPanel;
    btNew: TButton;
    btExit: TButton;
    btEdit: TButton;
    btDelete: TButton;
    procedure btEditClick(Sender: TObject);
    procedure btDeleteClick(Sender: TObject);
    procedure btNewClick(Sender: TObject);
    procedure btExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure GridDblClick(Sender: TObject);
  private
    { Private declarations }
    FController: TMaquinaController;
    function GetSelectedMaquina: TMaquina;
    procedure LoadMaquinas;
  public
    { Public declarations }
  end;

var
  frListarMaquinas: TfrListarMaquinas;

implementation

uses
   EditarMaquina, Security, Utils;

{$R *.dfm}

procedure TfrListarMaquinas.btDeleteClick(Sender: TObject);
var
  Maquina: TMaquina;
  Msg: string;
begin
  Maquina := GetSelectedMaquina;

  Msg := 'Tem certeza que voc� deseja excluir a M�quina "' + Maquina.Descricao + '"?';

  if MessageDlg(Msg, mtWarning, mbYesNo, 0) = mrYes then
  begin
    FController.DeleteMaquina(Maquina);
    LoadMaquinas;
  end;
end;

procedure TfrListarMaquinas.btEditClick(Sender: TObject);
var
  Maquina: TMaquina;
  frEditarMaquina: TfrEditarMaquina;
begin
  Maquina := GetSelectedMaquina;

  frEditarMaquina := TfrEditarMaquina.Create(Self);
  try
    frEditarMaquina.SetMaquina(Maquina.Id);
    frEditarMaquina.ShowModal;

    if frEditarMaquina.ModalResult = mrOk then
      LoadMaquinas;
  finally
    frEditarMaquina.Free;
  end;
end;

procedure TfrListarMaquinas.btExitClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrListarMaquinas.btNewClick(Sender: TObject);
var
  frEditarMaquina: TfrEditarMaquina;
begin
  frEditarMaquina := TfrEditarMaquina.Create(Self);
  try
    frEditarMaquina.ShowModal;

    if frEditarMaquina.ModalResult = mrOk then
      LoadMaquinas;
  finally
    frEditarMaquina.Free;
  end;
end;

procedure TfrListarMaquinas.FormCreate(Sender: TObject);
begin
  FController := TMaquinaController.Create;
  LoadMaquinas;
end;

procedure TfrListarMaquinas.FormDestroy(Sender: TObject);
begin
  FController.Free;
end;

function TfrListarMaquinas.GetSelectedMaquina: TMaquina;
begin
  Result := TMaquina(Grid.Cols[0].Objects[Grid.Row]);
end;

procedure TfrListarMaquinas.GridDblClick(Sender: TObject);
var
  Maquina: TMaquina;
begin
  Maquina := TMaquina(Grid.Cols[0].Objects[Grid.Row]);
end;

procedure TfrListarMaquinas.LoadMaquinas;
var
  Maquinas: TList<TMaquina>;
  Maquina: TMaquina;
  I, J: Integer;
begin
  for I := 0 to Grid.RowCount - 1 do
    for J := 0 to Grid.ColCount - 1 do
      Grid.Cells[J, I] := '';
  Grid.RowCount := 2;

  Grid.Cells[0, 0] := 'Descri��o';
  Grid.Cells[1, 0] := 'Endere�o';
  Grid.Cells[2, 0] := 'Porta';
  Grid.Cells[3, 0] := 'Usu�rio';
  Grid.Cells[4, 0] := 'Cliente';

  Maquinas := FController.GetAllMaquinas;
  try
    if Maquinas.Count > 0 then
    begin
      Grid.RowCount := 1 + Maquinas.Count;

      for I := 0 to Maquinas.Count - 1 do
      begin
        Maquina := Maquinas[I];
        Grid.Cols[0].Objects[I + 1] := Maquina;
        Grid.Cells[0, I + 1] := Maquina.Descricao;

        Grid.Cells[1, I + 1] := Maquina.Endereco;

        Grid.Cells[2, I + 1] := IntToStr(Maquina.Porta);

        Grid.Cells[3, I + 1] := Maquina.Usuario;

        Grid.Cells[4, I + 1] := Maquina.Cliente.Nome;

      end;
    end;
  finally
    Maquinas.Free;
  end;
end;

end.
