object frEditarCliente: TfrEditarCliente
  Left = 0
  Top = 0
  Caption = 'Cadastro de Cliente'
  ClientHeight = 131
  ClientWidth = 373
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelTop: TPanel
    Left = 0
    Top = 0
    Width = 373
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    Caption = 'Cliente'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
  end
  object MainPanel: TPanel
    Left = 0
    Top = 41
    Width = 373
    Height = 49
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      373
      49)
    object lbName: TLabel
      Left = 16
      Top = 13
      Width = 31
      Height = 13
      Caption = 'Nome:'
    end
    object edNome: TEdit
      Left = 64
      Top = 10
      Width = 297
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 0
    end
    object cbContinuarIncluindo: TCheckBox
      Left = 16
      Top = 32
      Width = 113
      Height = 17
      Caption = 'Continuar Incluindo'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
  end
  object PanelBottom: TPanel
    Left = 0
    Top = 90
    Width = 373
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      373
      41)
    object btSave: TButton
      Left = 175
      Top = 7
      Width = 90
      Height = 28
      Anchors = [akTop, akRight]
      Caption = '&Salvar'
      Default = True
      TabOrder = 0
      OnClick = btSaveClick
    end
    object btCancel: TButton
      Left = 271
      Top = 7
      Width = 90
      Height = 28
      Anchors = [akTop, akRight]
      Caption = '&Cancelar'
      TabOrder = 1
      OnClick = btCancelClick
    end
  end
end
