unit ListarMaquinasConexao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseForm, Data.DB, Vcl.Grids,
  Vcl.DBGrids, Vcl.StdCtrls, Vcl.ExtCtrls, Aurelius.Bind.Dataset, Usuario, MaquinaController;

type
  TfrmListarMaquinasConexao = class(TBaseForm)
    PanelBottom: TPanel;
    btExit: TButton;
    btConectar: TButton;
    PanelTop: TPanel;
    MainPanel: TPanel;
    dbgMain: TDBGrid;
    procedure FormCreate(Sender: TObject);
    procedure btConectarClick(Sender: TObject);
    procedure btExitClick(Sender: TObject);
  private
    dsMain: TDataSource;
    adsMain: TAureliusDataSet;
    FController: TMaquinaController;
    procedure ListarMaquinas;
    procedure OnGetTextCliente(Sender: TField; var Text: string; DisplayText: Boolean);
    procedure OnGetTextEmpresa(Sender: TField; var Text: string; DisplayText: Boolean);
  public
    { Public declarations }
  end;

var
  frmListarMaquinasConexao: TfrmListarMaquinasConexao;

implementation

{$R *.dfm}

uses DataSetAttribute, Utils, Security;

procedure TfrmListarMaquinasConexao.btConectarClick(Sender: TObject);
var
  Maquina: TMaquina;
  Command: AnsiString;
begin
  Maquina := adsMain.Current<TMaquina>;
  command := 'cmdkey.exe /generic:TERMSRV/'+Maquina.Endereco+ ' /user:'+ Maquina.Usuario+' /pass:'+ Crypt(caDecrypt, Maquina.Senha);
  WinExec(PAnsiChar(command), 0);
  ExecAndWait('mstsc.exe /v:'+Maquina.Endereco, 'C:\Windows');
  ShowMessage('Terminou');
  WinExec(PAnsiChar('cmdkey.exe /delete TERMSRV/' +Maquina.Endereco), 0);
end;

procedure TfrmListarMaquinasConexao.btExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmListarMaquinasConexao.FormCreate(Sender: TObject);
begin
  FController := TMaquinaController.Create;
  adsMain := TAureliusDataSet.Create(Self);
  dsMain := TDataSource.Create(Self);
  dsMain.DataSet := adsMain;
  dbgMain.DataSource := dsMain;
  ListarMaquinas;
end;

procedure TfrmListarMaquinasConexao.ListarMaquinas;
begin
  adsMain.Close;
  adsMain.SetSourceList(FController.GetAllMaquinas);
  adsMain.ObjectClass := TMaquina;
  adsMain.Open;
  DisplayDataSetFields(adsMain);
  adsMain.FieldByName('Self').Visible := False;
  adsMain.FieldByName('Cliente').OnGetText := OnGetTextCliente;
  adsMain.FieldByName('Empresa').OnGetText := OnGetTextEmpresa;
end;

procedure TfrmListarMaquinasConexao.OnGetTextCliente(Sender: TField; var Text: string; DisplayText: Boolean);
begin
  Text := TAureliusEntityField(Sender).AsEntity<TCliente>.Nome;
end;

procedure TfrmListarMaquinasConexao.OnGetTextEmpresa(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := TAureliusEntityField(Sender).AsEntity<TEmpresa>.Nome;
end;

end.
