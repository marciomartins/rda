unit EditarCliente;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseForm, Vcl.StdCtrls, Vcl.ExtCtrls, Usuario, ClienteController;

type
  TfrEditarCliente = class(TBaseForm)
    PanelTop: TPanel;
    MainPanel: TPanel;
    lbName: TLabel;
    edNome: TEdit;
    PanelBottom: TPanel;
    btSave: TButton;
    btCancel: TButton;
    cbContinuarIncluindo: TCheckBox;
    procedure btSaveClick(Sender: TObject);
    procedure btCancelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FController: TClienteController;

  public
    { Public declarations }
    procedure SetCliente(ClienteId: Variant);
  end;

var
  frEditarCliente: TfrEditarCliente;

implementation

{$R *.dfm}

procedure TfrEditarCliente.btCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrEditarCliente.btSaveClick(Sender: TObject);
var
  Cliente: TCliente;
begin
  Cliente := FController.Cliente;
  Cliente.Nome := edNome.Text;
  FController.Save(Cliente);
  if cbContinuarIncluindo.Checked then
  begin
    edNome.Text := '';
    edNome.SetFocus;
  end
  else
    ModalResult := mrOk;
end;

procedure TfrEditarCliente.FormCreate(Sender: TObject);
begin
  FController := TClienteController.Create;
end;

procedure TfrEditarCliente.FormDestroy(Sender: TObject);
begin
  FController.Free;
end;

procedure TfrEditarCliente.FormShow(Sender: TObject);
begin
  edNome.SetFocus;
end;

procedure TfrEditarCliente.SetCliente(ClienteId: Variant);
var
  Cliente: TCliente;
begin
  FController.Load(ClienteId);
  Cliente := FController.Cliente;

  edNome.Text := Cliente.Nome;
end;

end.
