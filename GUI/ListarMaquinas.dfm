object frListarMaquinas: TfrListarMaquinas
  Left = 0
  Top = 0
  Caption = 'Consulta de M'#225'quinas'
  ClientHeight = 300
  ClientWidth = 721
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object PanelTop: TPanel
    Left = 0
    Top = 0
    Width = 721
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    Caption = 'M'#225'quinas'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
  end
  object MainPanel: TPanel
    Left = 0
    Top = 41
    Width = 721
    Height = 218
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      721
      218)
    object Grid: TStringGrid
      Left = 7
      Top = 8
      Width = 706
      Height = 206
      Anchors = [akLeft, akTop, akRight, akBottom]
      DefaultRowHeight = 20
      FixedCols = 0
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRowSelect]
      ScrollBars = ssVertical
      TabOrder = 0
      OnDblClick = GridDblClick
      ColWidths = (
        191
        199
        64
        111
        123)
    end
  end
  object PanelBottom: TPanel
    Left = 0
    Top = 259
    Width = 721
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      721
      41)
    object btNew: TButton
      Left = 527
      Top = 7
      Width = 90
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Novo'
      TabOrder = 2
      OnClick = btNewClick
    end
    object btExit: TButton
      Left = 623
      Top = 7
      Width = 90
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Fechar'
      ModalResult = 2
      TabOrder = 3
      OnClick = btExitClick
    end
    object btEdit: TButton
      Left = 335
      Top = 7
      Width = 90
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Editar'
      Default = True
      TabOrder = 0
      OnClick = btEditClick
    end
    object btDelete: TButton
      Left = 431
      Top = 7
      Width = 90
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Deletar'
      TabOrder = 1
      OnClick = btDeleteClick
    end
  end
end
