unit EditarUsuario;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, BaseForm, Vcl.StdCtrls, Vcl.ExtCtrls, Usuario, UsuarioController,
  Vcl.CheckLst, System.Generics.Collections;

type
  TfrEditarUsuario = class(TBaseForm)
    PanelTop: TPanel;
    MainPanel: TPanel;
    lbName: TLabel;
    edNome: TEdit;
    PanelBottom: TPanel;
    btSave: TButton;
    btCancel: TButton;
    Label1: TLabel;
    edSenha: TEdit;
    lbEmpresas: TLabel;
    clbEmpresas: TCheckListBox;
    cbContinuarIncluindo: TCheckBox;
    procedure btSaveClick(Sender: TObject);
    procedure btCancelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);

  private
    { Private declarations }
    FController: TUsuarioController;
    FUsuario: TUsuario;
    OsExcluidos: TList<TUsuarioEmpresa>;
    function Contains(UsuarioEmpresa: TUsuarioEmpresa): Boolean;
    function Remove(UsuarioEmpresa: TUsuarioEmpresa): Boolean;
  public
    { Public declarations }
    procedure SetUsuario(UsuarioId: Variant);

  end;

  var
  frEditarUsuario: TfrEditarUsuario;

implementation

{$R *.dfm}

uses EmpresaController, Security;


procedure TfrEditarUsuario.btCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

function TfrEditarUsuario.Contains(UsuarioEmpresa: TUsuarioEmpresa): Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := 0 to FUsuario.UsuarioEmpresa.Count -1 do
  begin
    Result := FUsuario.UsuarioEmpresa[I].Empresa.Nome.Equals(UsuarioEmpresa.Empresa.Nome);
    if Result then
      Break;
  end;
end;

function TfrEditarUsuario.Remove(UsuarioEmpresa: TUsuarioEmpresa): Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := 0 to FUsuario.UsuarioEmpresa.Count -1 do
  begin
    Result := FUsuario.UsuarioEmpresa[I].Empresa.Nome.Equals(UsuarioEmpresa.Empresa.Nome);
    if Result then
    begin
      OsExcluidos.Add(FUsuario.UsuarioEmpresa[I]);
      Break;
    end;

  end;
end;

procedure TfrEditarUsuario.btSaveClick(Sender: TObject);
var
  I: Integer;
  UsuarioEmpresa: TUsuarioEmpresa;
  Excluido: TUsuarioEmpresa;
begin
  if FUsuario = nil then
    FUsuario := FController.Usuario;

  FUsuario.Nome := edNome.Text;
  fUsuario.Senha := Crypt(caCrypt, edSenha.Text);

  OsExcluidos.Clear;
  for I := 0 to clbEmpresas.Items.Count-1 do
  begin
    UsuarioEmpresa := TUsuarioEmpresa.Create;
    UsuarioEmpresa.Usuario := FUsuario;
    UsuarioEmpresa.Empresa := TEmpresa(clbEmpresas.Items.Objects[I]);

    if clbEmpresas.Checked[I] then
    begin
      if not Contains(UsuarioEmpresa) then
        FUsuario.UsuarioEmpresa.Add(UsuarioEmpresa);
    end
    else if Contains(UsuarioEmpresa) then
      Remove(UsuarioEmpresa);
  end;

  FController.Save(FUsuario);

  for Excluido in OsExcluidos do
    FController.Manager.Remove(Excluido);
  OsExcluidos.Clear;

  if cbContinuarIncluindo.Checked then
  begin
    edNome.Text := '';
    edSenha.Text := '';
    for I:=0 to FUsuario.UsuarioEmpresa.Count -1 do
      clbEmpresas.Checked[I] := False;
    edNome.SetFocus;
  end
  else
    ModalResult := mrOk;
end;

procedure TfrEditarUsuario.FormCreate(Sender: TObject);
var
  Empresas: TList<TEmpresa>;
  Empresa: TEmpresa;
begin
  OsExcluidos := TList<TUsuarioEmpresa>.Create;
  FController := TUsuarioController.Create;
  Empresas := FController.GetEmpresas;
  for Empresa in Empresas do
    clbEmpresas.Items.AddObject(Empresa.Nome, Empresa);
end;

procedure TfrEditarUsuario.FormDestroy(Sender: TObject);
begin
  FController.Free;
  OsExcluidos.Free;
end;

procedure TfrEditarUsuario.FormShow(Sender: TObject);
var
  I: Integer;
  Indice: Integer;
begin
  if FUsuario = nil then
    Exit;
  for I := 0 to FUsuario.UsuarioEmpresa.Count -1 do
  begin
    Indice := clbEmpresas.Items.IndexOf(FUsuario.UsuarioEmpresa[I].Empresa.Nome);
    if indice >= 0 then
      clbEmpresas.Checked[Indice] := True;
  end;
  edNome.SetFocus;
end;

procedure TfrEditarUsuario.SetUsuario(UsuarioId: Variant);
begin
  FController.Load(UsuarioId);
  FUsuario := FController.Usuario;
  edNome.Text := FUsuario.Nome;
  edSenha.Text := Crypt(caDecrypt, FUsuario.Senha);
end;

end.
