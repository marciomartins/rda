object frListarUsuarios: TfrListarUsuarios
  Left = 0
  Top = 0
  Caption = 'Consulta de Usu'#225'rios'
  ClientHeight = 212
  ClientWidth = 395
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object PanelTop: TPanel
    Left = 0
    Top = 0
    Width = 395
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    Caption = 'Usu'#225'rios'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
  end
  object MainPanel: TPanel
    Left = 0
    Top = 41
    Width = 395
    Height = 130
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      395
      130)
    object Grid: TStringGrid
      Left = 7
      Top = 8
      Width = 380
      Height = 118
      Anchors = [akLeft, akTop, akRight, akBottom]
      ColCount = 2
      DefaultRowHeight = 20
      FixedCols = 0
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRowSelect]
      ScrollBars = ssVertical
      TabOrder = 0
      OnDblClick = GridDblClick
      ColWidths = (
        186
        176)
    end
  end
  object PanelBottom: TPanel
    Left = 0
    Top = 171
    Width = 395
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      395
      41)
    object btNew: TButton
      Left = 201
      Top = 7
      Width = 90
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Novo'
      TabOrder = 2
      OnClick = btNewClick
    end
    object btExit: TButton
      Left = 297
      Top = 7
      Width = 90
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Fechar'
      ModalResult = 2
      TabOrder = 3
      OnClick = btExitClick
    end
    object btEdit: TButton
      Left = 9
      Top = 7
      Width = 90
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Editar'
      Default = True
      TabOrder = 0
      OnClick = btEditClick
    end
    object btDelete: TButton
      Left = 105
      Top = 7
      Width = 90
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Deletar'
      TabOrder = 1
      OnClick = btDeleteClick
    end
  end
end
