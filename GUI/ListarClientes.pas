unit ListarClientes;

interface

uses
  Generics.Collections, Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Grids, Vcl.ExtCtrls, BaseForm, Usuario, ClienteController,
  Aurelius.Bind.Dataset, Data.DB, Vcl.DBGrids;

type
  TfrListarClientes = class(TBaseForm)
    PanelTop: TPanel;
    MainPanel: TPanel;
    PanelBottom: TPanel;
    btNew: TButton;
    btExit: TButton;
    btEdit: TButton;
    btDelete: TButton;
    dgMain: TDBGrid;
    dgMaquinas: TDBGrid;
    Splitter1: TSplitter;
    procedure btEditClick(Sender: TObject);
    procedure btDeleteClick(Sender: TObject);
    procedure btNewClick(Sender: TObject);
    procedure btExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure GridClick(Sender: TObject);
  private
    { Private declarations }
    dsMain: TDataSource;
    dsMaquinas: TDataSource;
    adsMain: TAureliusDataSet;
    adsMaquinas: TAureliusDataSet;
    FController: TClienteController;
    procedure LoadClientes;
    procedure ListarMaquinas;
  public
    { Public declarations }
  end;

var
  frListarClientes: TfrListarClientes;

implementation

uses
  EditarCliente, DataSetAttribute;

{$R *.dfm}

procedure TfrListarClientes.btDeleteClick(Sender: TObject);
var
  Cliente: TCliente;
  Msg: string;
begin
  Cliente := adsMain.Current<TCliente>;
  Msg := 'Tem certeza que voc� deseja excluir o Cliente "' + Cliente.Nome + '"?';

  if MessageDlg(Msg, mtWarning, mbYesNo, 0) = mrYes then
  begin
    FController.DeleteCliente(Cliente);
    LoadClientes;
  end;
end;

procedure TfrListarClientes.btEditClick(Sender: TObject);
var
  Cliente: TCliente;
  frEditarCliente: TfrEditarCliente;
begin
  Cliente := adsMain.Current<TCliente>;

  frEditarCliente := TfrEditarCliente.Create(Self);
  try
    frEditarCliente.SetCliente(Cliente.Id);
    frEditarCliente.ShowModal;

    if frEditarCliente.ModalResult = mrOk then
      LoadClientes;
  finally
    frEditarCliente.Free;
  end;
end;

procedure TfrListarClientes.btExitClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrListarClientes.btNewClick(Sender: TObject);
var
  frEditarCliente: TfrEditarCliente;
begin
  frEditarCliente := TfrEditarCliente.Create(Self);
  try
    frEditarCliente.ShowModal;

    if frEditarCliente.ModalResult = mrOk then
      LoadClientes;
  finally
    frEditarCliente.Free;
  end;
end;

procedure TfrListarClientes.ListarMaquinas;
begin
  //Op��o 1
  adsMaquinas.Close;
  adsMaquinas.DatasetField := adsMain.FieldByName('Maquina') as TDataSetField;
  adsMaquinas.Open;

  //Op��o 2
  //adsMaquinas.Close;
  //adsMaquinas.SetSourceList(adsMain.Current<TCliente>.Maquina);
  //adsMaquinas.ObjectClass := TMaquina;
  //adsMaquinas.Open;
  adsMaquinas.FieldByName('Self').Visible := False;
  adsMaquinas.FieldByName('Id').Visible := False;
  adsMaquinas.FieldByName('Cliente').Visible := False;
end;

procedure TfrListarClientes.FormCreate(Sender: TObject);
begin
  FController := TClienteController.Create;
  dsMain := TDataSource.Create(Self);
  dsMaquinas := TDataSource.Create(Self);
  adsMain := TAureliusDataset.Create(Self);
  adsMaquinas := TAureliusDataset.Create(Self);
  dsMain.DataSet := adsMain;
  dsMaquinas.DataSet := adsMaquinas;
  dgMain.DataSource := dsMain;
  dgMaquinas.DataSource := dsMaquinas;
  LoadClientes;
end;

procedure TfrListarClientes.FormDestroy(Sender: TObject);
begin
  FController.Free;
end;

procedure TfrListarClientes.GridClick(Sender: TObject);
begin
  btEditClick(Sender);
end;

procedure TfrListarClientes.LoadClientes;
begin
  adsMain.Close;
  adsMain.SetSourceList(FController.GetAllClientes);
  adsMain.ObjectClass := TCliente;
  adsMain.Open;
  DisplayDataSetFields(adsMain);
  adsMain.FieldByName('Self').Visible := False;
  //adsMain.FieldByName('ID').Visible := False;
  adsMain.FieldByName('Maquina').Visible := False;
  //adsMain.FieldByName('Nome').DisplayLabel := 'Nome';
  //adsMain.FieldByName('Nome').DisplayWidth := 55;

  adsMaquinas.DatasetField := adsMain.FieldByName('Maquina') as TDataSetField;
  adsMaquinas.Open;
  adsMaquinas.FieldByName('Self').Visible := False;
  adsMaquinas.FieldByName('Id').Visible := False;
  adsMaquinas.FieldByName('Cliente').Visible := False;
  adsMaquinas.FieldByName('Descricao').DisplayWidth := 30;
end;


end.
