unit MaquinaController;

interface

uses
  System.Classes,
  System.SysUtils,
  Generics.Collections,
  Aurelius.Engine.ObjectManager,
  Aurelius.Criteria.Expression,
  Aurelius.Criteria.Linq,
  Settings,
  Usuario;

type
  TMaquinaController = class
  private
    FManager: TObjectManager;
    FMaquina: TMaquina;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Save(Maquina: TMaquina);
    procedure Load(MaquinaId: Variant);
    procedure DeleteMaquina(Maquina: TMaquina);
    function GetClientes: TList<TCliente>;
    function GetEmpresas: TList<TEmpresa>;
    function GetAllMaquinas: TList<TMaquina>;
    property Maquina: TMaquina read FMaquina;
  end;

implementation

uses
  DBConnection;

{ TMusicasController }

constructor TMaquinaController.Create;
begin
  FMaquina := TMaquina.Create;
  FManager := TDBConnection.GetInstance.CreateObjectManager;
end;

procedure TMaquinaController.DeleteMaquina(Maquina: TMaquina);
begin
  if not FManager.IsAttached(Maquina) then
    Maquina := FManager.Find<TMaquina>(Maquina.Id);
  FManager.Remove(Maquina);
end;

destructor TMaquinaController.Destroy;
begin
  if not FManager.IsAttached(FMaquina) then
    FMaquina.Free;
  FManager.Free;
  inherited;
end;

function TMaquinaController.GetAllMaquinas: TList<TMaquina>;
var
  UsuariosEmpresas: Tlist<TUsuarioEmpresa>;
  UsuarioEmpresa: TUsuarioEmpresa;
  L: TStrings;
begin
  FManager.Clear;
  if TSettings.Instance.Usuario.Id = -1 then
    Result := FManager.FindAll<TMaquina>
  else
  begin
    UsuariosEmpresas := FManager.Find<TUsuarioEmpresa>.Add(TExpression.Eq('Usuario', TSettings.Instance.Usuario.Id)).List;
    L := TStringList.Create;
    try
      for UsuarioEmpresa in UsuariosEmpresas do
        l.Add(IntToStr(UsuarioEmpresa.Empresa.ID));

      Result := FManager.Find<TMaquina>.Add(TLinq.Sql('C.ID IN ('+L.CommaText+')')).List;
    finally
      L.Free;
    end;
  end;
end;

function TMaquinaController.GetClientes: TList<TCliente>;
begin
  Result := FManager.FindAll<TCliente>;
end;

function TMaquinaController.GetEmpresas: TList<TEmpresa>;
begin
  Result := FManager.FindAll<TEmpresa>;
end;

procedure TMaquinaController.Load(MaquinaId: Variant);
begin
  if not FManager.IsAttached(FMaquina) then
    FMaquina.Free;
  FMaquina := FManager.Find<TMaquina>(MaquinaId);
end;

procedure TMaquinaController.Save(Maquina: TMaquina);
begin
  if not FManager.IsAttached(Maquina) then
    FManager.SaveOrUpdate(Maquina);
  FManager.Flush;
end;

end.
