unit EmpresaController;

interface

uses
  Generics.Collections,
  Usuario,
  Aurelius.Engine.ObjectManager;

type
  TEmpresaController = class
  private
    FManager: TObjectManager;
    FEmpresa: TEmpresa;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Save(Empresa: TEmpresa);
    procedure Load(EmpresaId: Variant);
    procedure DeleteEmpresa(Empresa: TEmpresa);
    function GetUsuarios: TList<TUsuario>;
    function GetAllEmpresas: TList<TEmpresa>;
    property Empresa: TEmpresa read FEmpresa;
  end;

implementation

uses
  DBConnection;

{ TMusicasController }

constructor TEmpresaController.Create;
begin
  FEmpresa := TEmpresa.Create;
  FManager := TDBConnection.GetInstance.CreateObjectManager;
end;

procedure TEmpresaController.DeleteEmpresa(Empresa: TEmpresa);
begin
  if not FManager.IsAttached(Empresa) then
    Empresa := FManager.Find<TEmpresa>(Empresa.Id);
  FManager.Remove(Empresa);
end;

destructor TEmpresaController.Destroy;
begin
  if not FManager.IsAttached(FEmpresa) then
    FEmpresa.Free;
  FManager.Free;
  inherited;
end;

function TEmpresaController.GetAllEmpresas: TList<TEmpresa>;
begin
  FManager.Clear;
  Result := FManager.FindAll<TEmpresa>;
end;

function TEmpresaController.GetUsuarios: TList<TUsuario>;
begin
  Result := FManager.FindAll<TUsuario>;
end;

procedure TEmpresaController.Load(EmpresaId: Variant);
begin
  if not FManager.IsAttached(FEmpresa) then
    FEmpresa.Free;
  FEmpresa := FManager.Find<TEmpresa>(EmpresaId);
end;

procedure TEmpresaController.Save(Empresa: TEmpresa);
begin
  if not FManager.IsAttached(Empresa) then
    FManager.SaveOrUpdate(Empresa);
  FManager.Flush;
end;

end.
