unit ClienteController;
interface

uses
  Generics.Collections,
  Usuario,
  Aurelius.Engine.ObjectManager;

type
  TClienteController = class
  private
    FManager: TObjectManager;
    FCliente: TCliente;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Save(Cliente: TCliente);
    procedure Load(ClienteId: Variant);
    procedure DeleteCliente(Cliente: TCliente);
    function GetAllClientes: TList<TCliente>;
    property Cliente: TCliente read FCliente;
  end;

implementation

uses
  DBConnection;

{ TMediaFilesController }

constructor TClienteController.Create;
begin
  FCliente := TCliente.Create;
  FManager := TDBConnection.GetInstance.CreateObjectManager;
end;

procedure TClienteController.DeleteCliente(Cliente: TCliente);
begin
  if not FManager.IsAttached(Cliente) then
    Cliente := FManager.Find<TCliente>(Cliente.Id);
  FManager.Remove(Cliente);
end;

destructor TClienteController.Destroy;
begin
  if not FManager.IsAttached(FCliente) then
    FCliente.Free;
  FManager.Free;
  inherited;
end;

function TClienteController.GetAllClientes: TList<TCliente>;
begin
  FManager.Clear;
  Result := FManager.FindAll<TCliente>;
end;

procedure TClienteController.Load(ClienteId: Variant);
begin
  if not FManager.IsAttached(FCliente) then
    FCliente.Free;
  FCliente := FManager.Find<TCliente>(ClienteId);
end;

procedure TClienteController.Save(Cliente: TCliente);
begin
  if not FManager.IsAttached(Cliente) then
    FManager.SaveOrUpdate(Cliente);
  FManager.Flush;
end;



end.
