unit UsuarioController;

interface

uses
  Generics.Collections,
  Usuario,
  Aurelius.Engine.ObjectManager,
  Aurelius.Criteria.Expression;

type
  TUsuarioController = class
  private
    FManager: TObjectManager;
    FUsuario: TUsuario;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Save(Usuario: TUsuario);
    procedure Load(UsuarioId: Variant);
    procedure DeleteUsuario(Usuario: TUsuario);
    function GetEmpresas: TList<TEmpresa>;
    function GetAllUsuarios: TList<TUsuario>;
    function FindByName(const Nome: string):TUsuario;
    property Usuario: TUsuario read FUsuario;
    property Manager: TObjectManager read FManager write FManager;
  end;

implementation

uses
  DBConnection;

{ TMediaFilesController }

constructor TUsuarioController.Create;
begin
  FUsuario := TUsuario.Create;
  FManager := TDBConnection.GetInstance.CreateObjectManager;
end;

procedure TUsuarioController.DeleteUsuario(Usuario: TUsuario);
begin
  if not FManager.IsAttached(Usuario) then
    Usuario := FManager.Find<TUsuario>(Usuario.Id);
  FManager.Remove(Usuario);
end;

destructor TUsuarioController.Destroy;
begin
  if not FManager.IsAttached(FUsuario) then
    FUsuario.Free;
  FManager.Free;
  inherited;
end;

function TUsuarioController.FindByName(const Nome: string): TUsuario;
begin
  Result := FManager.Find<TUsuario>.Add(TExpression.Eq('NOME', Nome)).UniqueResult;
end;

function TUsuarioController.GetAllUsuarios: TList<TUsuario>;
begin
  FManager.Clear;
  Result := FManager.FindAll<TUsuario>;
end;

function TUsuarioController.GetEmpresas: TList<TEmpresa>;
begin
  Result := FManager.FindAll<TEmpresa>;
end;

procedure TUsuarioController.Load(UsuarioId: Variant);
begin
  if not FManager.IsAttached(FUsuario) then
    FUsuario.Free;
  FUsuario := FManager.Find<TUsuario>(UsuarioId);
end;

procedure TUsuarioController.Save(Usuario: TUsuario);
begin
  if not FManager.IsAttached(Usuario) then
    FManager.SaveOrUpdate(Usuario);
  FManager.Flush;
end;

end.
