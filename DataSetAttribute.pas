unit DataSetAttribute;

interface

uses System.RTTI, Aurelius.Bind.Dataset, Data.DB;

type
  DisplayAttribute = class(TCustomAttribute)
  private
    FText: string;
    FWidth: Integer;
    FVisible: Boolean;
  public
    constructor Create(const AText: string); overload;
    constructor Create(const AText: string; AWidth: Integer); overload;
    constructor Create(const AText: string; AWidth: Integer; AVisible: Boolean); overload;
    property Text: string read FText write FText;
    property Width: Integer read FWidth write FWidth;
    property Visible: Boolean read FVisible write FVisible;
  end;

procedure DisplayDataSetFields(Dataset: TAureliusDataSet);

implementation

procedure DisplayDataSetFields(Dataset: TAureliusDataSet);
var
  c: TRttiContext;
  t: TRttiType;
  p: TRttiProperty;
  a: TCustomAttribute;
  Value: TValue;
  Field: TField;
begin
  c := TRttiContext.Create;
  try
    t := c.GetType(Dataset.ObjectClass);
    for p in t.getProperties do
      for a in p.GetAttributes do
        if a is DisplayAttribute then
        begin
          // Value := p.GetValue(Obj);
          Field := Dataset.FieldByName(p.Name);
          Field.DisplayLabel := DisplayAttribute(a).Text;
          if DisplayAttribute(a).Width > 0 then
            Field.DisplayWidth := DisplayAttribute(a).Width;
          Field.Visible := DisplayAttribute(a).Visible;
          // p.Name, DisplayLabelAttribute(a).Text, Value.ToString;
        end;
  finally
    c.Free;
  end;
end;

{ DisplayLabelAttribute }

constructor DisplayAttribute.Create(const AText: string);
begin
  FText := AText;
  FWidth := 0;
  FVisible := True;
end;

constructor DisplayAttribute.Create(const AText: string; AWidth: Integer);
begin
  FText := AText;
  FWidth := AWidth;
  FVisible := True;
end;

constructor DisplayAttribute.Create(const AText: string; AWidth: Integer;
  AVisible: Boolean);
begin
  FText := AText;
  FWidth := AWidth;
  FVisible := AVisible;
end;

end.
