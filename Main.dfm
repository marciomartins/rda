object FormMain: TFormMain
  Left = 0
  Top = 0
  Caption = 'RDA - Beta'
  ClientHeight = 300
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIForm
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object MainMenu1: TMainMenu
    Left = 32
    Top = 40
    object Cadastro1: TMenuItem
      Caption = 'Cadastro'
      OnClick = Cadastro1Click
      object CadastroUsuario: TMenuItem
        Caption = 'Usu'#225'rio'
        OnClick = CadastroUsuarioClick
      end
      object CadastroEmpresa: TMenuItem
        Caption = 'Empresa'
        OnClick = CadastroEmpresaClick
      end
      object CadastroCliente: TMenuItem
        Caption = 'Cliente'
        OnClick = CadastroClienteClick
      end
      object CadastroMaquina: TMenuItem
        Caption = 'M'#225'quina'
        OnClick = CadastroMaquinaClick
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Sair1: TMenuItem
        Caption = 'Sair'
        OnClick = Sair1Click
      end
    end
    object Consulta1: TMenuItem
      Caption = 'Consulta'
      object ConsultaUsuario: TMenuItem
        Caption = 'Usu'#225'rio'
        OnClick = ConsultaUsuarioClick
      end
      object ConsultaEmpresa: TMenuItem
        Caption = 'Empresa'
        OnClick = ConsultaEmpresaClick
      end
      object ConsultaCliente: TMenuItem
        Caption = 'Cliente'
        OnClick = ConsultaClienteClick
      end
      object ConsultaMquina: TMenuItem
        Caption = 'M'#225'quina'
        OnClick = ConsultaMquinaClick
      end
      object ListarMaquinasConexao: TMenuItem
        Caption = 'Maquinas Conex'#227'o'
        OnClick = ListarMaquinasConexaoClick
      end
    end
    object Util: TMenuItem
      Caption = 'Util'
      object SQLMonitor: TMenuItem
        Caption = 'SQL Monitor'
        OnClick = SQLMonitorClick
      end
    end
  end
end
