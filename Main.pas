unit Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, ShellApi,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus, Vcl.StdCtrls;

type
  TFormMain = class(TForm)
    MainMenu1: TMainMenu;
    Cadastro1: TMenuItem;
    Sair1: TMenuItem;
    N1: TMenuItem;
    CadastroCliente: TMenuItem;
    CadastroMaquina: TMenuItem;
    Consulta1: TMenuItem;
    ConsultaCliente: TMenuItem;
    ConsultaMquina: TMenuItem;
    CadastroUsuario: TMenuItem;
    ConsultaUsuario: TMenuItem;
    CadastroEmpresa: TMenuItem;
    ConsultaEmpresa: TMenuItem;
    Util: TMenuItem;
    SQLMonitor: TMenuItem;
    ListarMaquinasConexao: TMenuItem;
    procedure Sair1Click(Sender: TObject);
    procedure CadastroClienteClick(Sender: TObject);
    procedure CadastroMaquinaClick(Sender: TObject);
	procedure FormShow(Sender: TObject);
    procedure ConsultaClienteClick(Sender: TObject);
    procedure ConsultaMquinaClick(Sender: TObject);
    procedure CadastroUsuarioClick(Sender: TObject);
    procedure ConsultaUsuarioClick(Sender: TObject);
    procedure CadastroEmpresaClick(Sender: TObject);
    procedure ConsultaEmpresaClick(Sender: TObject);
    procedure SQLMonitorClick(Sender: TObject);
    procedure Cadastro1Click(Sender: TObject);
    procedure ListarMaquinasConexaoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormMain: TFormMain;

implementation

{$R *.dfm}

uses Utils, DBConnection, Aurelius.Engine.DatabaseManager, EditarCliente, EditarMaquina, Login,
  Settings, ListarClientes, ListarMaquinas, EditarUsuario, ListarUsuarios,
  ConnectionSettings, EditarEmpresa, ListarEmpresas, SqlMonitor,
  ListarMaquinasConexao;

procedure TFormMain.Cadastro1Click(Sender: TObject);
begin
  CadastroUsuario.Visible := TSettings.Instance.Usuario.Nome = UsuarioMaster;
  CadastroEmpresa.Visible := TSettings.Instance.Usuario.Nome = UsuarioMaster;
  CadastroCliente.Visible := TSettings.Instance.Usuario.Nome = UsuarioMaster;
  CadastroMaquina.Visible := TSettings.Instance.Usuario.Nome = UsuarioMaster;
  ConsultaUsuario.Visible := TSettings.Instance.Usuario.Nome = UsuarioMaster;
  ConsultaEmpresa.Visible := TSettings.Instance.Usuario.Nome = UsuarioMaster;
  ConsultaCliente.Visible := TSettings.Instance.Usuario.Nome = UsuarioMaster;
  ConsultaMquina.Visible := TSettings.Instance.Usuario.Nome = UsuarioMaster;
  Util.Visible := TSettings.Instance.Usuario.Nome = UsuarioMaster;
  SQLMonitor.Visible := TSettings.Instance.Usuario.Nome = UsuarioMaster;
end;

procedure TFormMain.CadastroClienteClick(Sender: TObject);
var
  frEditarCliente: TfrEditarCliente;
begin
  frEditarCliente := TfrEditarCliente.Create(Self);
  try
    frEditarCliente.ShowModal;
  finally
    frEditarCliente.Free;
  end;
end;

procedure TFormMain.ConsultaClienteClick(Sender: TObject);
var
  frListarClientes: TfrListarClientes;
begin
  frListarClientes := TfrListarClientes.Create(Self);
  try
    frListarClientes.ShowModal;
  finally
    frListarClientes.Free;
  end;
end;

procedure TFormMain.CadastroEmpresaClick(Sender: TObject);
var
  frEditarEmpresa: TfrEditarEmpresa;
begin
  frEditarEmpresa := TfrEditarEmpresa.Create(Self);
  try
    frEditarEmpresa.ShowModal;
  finally
    frEditarEmpresa.Free;
  end;
end;

procedure TFormMain.ConsultaEmpresaClick(Sender: TObject);
var
  frListarEmpresas: TfrListarEmpresas;
begin
  frListarEmpresas := TfrListarEmpresas.Create(Self);
  try
    frListarEmpresas.ShowModal;
  finally
    frListarEmpresas.Free;
  end;
end;

procedure TFormMain.CadastroMaquinaClick(Sender: TObject);
var
  frEditarMaquina: TfrEditarMaquina;
begin
  frEditarMaquina := TfrEditarMaquina.Create(Self);
  try
    frEditarMaquina.ShowModal;
  finally
    frEditarMaquina.Free;
  end;
end;

procedure TFormMain.ConsultaMquinaClick(Sender: TObject);
var
  frListarMaquinas: TfrListarMaquinas;
begin
  frListarMaquinas := TfrListarMaquinas.Create(Self);
  try
    frListarMaquinas.ShowModal;
  finally
    frListarMaquinas.Free;
  end;
end;

procedure TFormMain.FormShow(Sender: TObject);
var
  Form: TfrLogin;
begin
  TfrConexao.LoadFromConfigFile;
  Form := TfrLogin.Create(nil);
  try
    if Form.ShowModal = mrOk then
    begin
      TSettings.Instance.Usuario.Nome := (Form.leUsuario.Text);
    end
    else
      Close;
  finally
    Form.Free;
  end;
  TDBConnection.GetInstance.AddCommandListener(TFrmSqlMonitor.GetInstance);
end;

procedure TFormMain.ListarMaquinasConexaoClick(Sender: TObject);
var
  Form: TfrmListarMaquinasConexao;
begin
  Form := TfrmListarMaquinasConexao.Create(self);
  Form.Show;
end;

procedure TFormMain.Sair1Click(Sender: TObject);
begin
  Close;
end;

procedure TFormMain.SQLMonitorClick(Sender: TObject);
begin
    TFrmSqlMonitor.GetInstance.Show;
end;

procedure TFormMain.CadastroUsuarioClick(Sender: TObject);
var
  frEditarUsuario: TfrEditarUsuario;
begin
  frEditarUsuario := TfrEditarUsuario.Create(Self);
  try
    frEditarUsuario.ShowModal;
  finally
    frEditarUsuario.Free;
  end;
end;

procedure TFormMain.ConsultaUsuarioClick(Sender: TObject);
var
  frListarUsuarios: TfrListarUsuarios;
begin
  frListarUsuarios := TfrListarUsuarios.Create(Self);
  try
    frListarUsuarios.ShowModal;
  finally
    frListarUsuarios.Free;
  end;
end;

end.
