unit ConnectionSettings;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Imaging.jpeg, IniFiles, DBConnection, Settings, Security, Vcl.ComCtrls,
  Vcl.Buttons;

type
  TfrConexao = class(TForm)
    PanelBottom: TPanel;
    btOk: TButton;
    btCancel: TButton;
    Panel1: TPanel;
    Panel2: TPanel;
    Image1: TImage;
    cbLibrary: TComboBox;
    Label1: TLabel;
    btTestar: TButton;
    PageControl: TPageControl;
    tsLibrarySQLite: TTabSheet;
    tsLibraryMSSQL: TTabSheet;
    leServidor: TLabeledEdit;
    leBanco: TLabeledEdit;
    leUsuario: TLabeledEdit;
    leSenha: TLabeledEdit;
    leBancoSQLite: TLabeledEdit;
    SpeedButton1: TSpeedButton;
    OpenDialog: TOpenDialog;
    Button1: TButton;
    procedure btOkClick(Sender: TObject);
    procedure btTestarClick(Sender: TObject);
    procedure btCancelClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure cbLibraryChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    procedure SaveToConfigFile;
    procedure SetPage;
    procedure LoadConfig;
    procedure SaveConfig;
  public
    class procedure LoadFromConfigFile;
  end;

var
  frConexao: TfrConexao;

implementation

{$R *.dfm}

procedure TfrConexao.btCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TfrConexao.btOkClick(Sender: TObject);
begin
  SaveConfig;
  SaveToConfigFile;
  Close;
end;

procedure TfrConexao.btTestarClick(Sender: TObject);
begin
  TDBConnection.GetInstance.BuildDatabase;
end;


procedure TfrConexao.Button1Click(Sender: TObject);
begin
  TDBConnection.GetInstance.DestroyDatabase;
end;

procedure TfrConexao.LoadConfig;
begin
  cbLibrary.ItemIndex := Ord(TSettings.Instance.Database.SGBD);
  leServidor.Text := TSettings.Instance.Database.Servidor;
  if TSettings.Instance.Database.SGBD = dbdSQLite then
    leBancoSQLite.Text := TSettings.Instance.Database.Banco
  else
    leBanco.Text := TSettings.Instance.Database.Banco;
  leUsuario.Text := TSettings.Instance.Database.Usuario;
  leSenha.Text := TSettings.Instance.Database.Senha;
end;

procedure TfrConexao.SetPage;
var
  PageToActivate: TTabSheet;
  I: Integer;
begin
  case cbLibrary.ItemIndex of
    0: PageToActivate := tsLibrarySQLite;
    1: PageToActivate := tsLibraryMSSQL;
  end;
  for I := 0 to PageControl.PageCount -1 do
    PageControl.Pages[I].TabVisible := PageControl.Pages[I] = PageToActivate;
end;

procedure TfrConexao.cbLibraryChange(Sender: TObject);
begin
  SetPage;
end;

procedure TfrConexao.FormCreate(Sender: TObject);
var
  Driver: TSGBD;
  I: Integer;
begin
  for Driver := Low(TSGBD) to High(TSGBD) do
    cbLibrary.Items.Add(Driver.ToString);
  for I := 0 to PageControl.PageCount -1 do
    PageControl.Pages[I].TabVisible := False;
  LoadFromConfigFile;
  cbLibrary.ItemIndex := Ord(TSettings.Instance.Database.SGBD);
  SetPage;
end;

procedure TfrConexao.FormShow(Sender: TObject);
begin
  LoadConfig;
end;

class procedure TfrConexao.LoadFromConfigFile;
var
  IniFile: TMemIniFile;
  SGBD: Integer;
  Driver: Integer;
begin
  IniFile := TMemIniFile.Create(TSettings.Instance.ConfigFile);
  try
    SGBD := IniFile.ReadInteger('DataBase', 'SGBD', -1);
    if SGBD in [0,1] then
      TSettings.Instance.Database.SGBD := TSGBD(SGBD);
    Driver := IniFile.ReadInteger('DataBase', 'Driver', -1);
    if Driver in [0,1] then
      TSettings.Instance.Database.Driver := TAureliusDriver(Driver);
    TSettings.Instance.Database.Servidor := IniFile.ReadString('DataBase', 'Servidor', '');
    TSettings.Instance.Database.Banco := IniFile.ReadString('DataBase', 'Banco', '');
    TSettings.Instance.Database.Usuario := IniFile.ReadString('DataBase', 'Usuario', '');
    TSettings.Instance.Database.Senha := IniFile.ReadString('DataBase', 'Senha', '');
  finally
    IniFile.Free;
  end;
end;

procedure TfrConexao.SaveConfig;
begin
  if cbLibrary.ItemIndex = 0 then
    TSettings.Instance.Database.Driver := adSQLite
  else
    TSettings.Instance.Database.Driver := adFiredac;
  TSettings.Instance.Database.SGBD := TSGBD(cbLibrary.ItemIndex);
  TSettings.Instance.Database.Servidor := leServidor.Text;
  if TSGBD(cbLibrary.ItemIndex) = dbdSQLite then
    TSettings.Instance.Database.Banco := leBancoSQLite.Text
  else
  begin
    TSettings.Instance.Database.Banco := leBanco.Text;
    TSettings.Instance.Database.Usuario := leUsuario.Text;
    TSettings.Instance.Database.Senha := leSenha.Text;
  end;
end;

procedure TfrConexao.SaveToConfigFile;
var
  IniFile: TMemIniFile;
begin
  IniFile := TMemIniFile.Create(TSettings.Instance.ConfigFile);
  try
    IniFile.WriteInteger('DataBase', 'Driver', Ord(TSettings.Instance.Database.Driver));
    IniFile.WriteInteger('DataBase', 'SGBD', Ord(TSettings.Instance.Database.SGBD));
    IniFile.WriteString('DataBase', 'Servidor', TSettings.Instance.Database.Servidor);
    IniFile.WriteString('DataBase', 'Banco', TSettings.Instance.Database.Banco);
    IniFile.WriteString('DataBase', 'Usuario', TSettings.Instance.Database.Usuario);
    IniFile.WriteString('DataBase', 'Senha', TSettings.Instance.Database.Senha);
    IniFile.UpdateFile;
  finally
    IniFile.Free;
  end;
end;

procedure TfrConexao.SpeedButton1Click(Sender: TObject);
begin
  if OpenDialog.Execute then
    leBancoSQLite.Text := OpenDialog.FileName;
end;

end.
