unit Usuario;

interface

uses
  Generics.Collections,
  DataSetAttribute,
  Aurelius.Mapping.Attributes,
  Aurelius.Types.Proxy;

type

  //TEmpresaUsuario = class;
  //http://www.tmssoftware.com/SITE/forum/forum_posts.asp?TID=4185&title=manytomany-mapping-and-saving

  TUsuarioEmpresa = class;
  TCliente = class;
  TEmpresa = class;

  [Entity]
  [Table('MAQUINA')]
  [Sequence('SEQ_MAQUINA')]
  [Id('FId', TIdGenerator.IdentityOrSequence)]
  TMaquina = class
  private
    [Column('ID', [TColumnProp.Unique, TColumnProp.Required, TColumnProp.NoUpdate])]
    FId: Integer;
    FDescricao: string;
    FEndereco: string;
    FPorta: Integer;
    FUsuario: string;
    FSenha: string;

    [Association([TAssociationProp.Required], [])]
    [JoinColumn('ID_CLIENTE', [TColumnProp.Required])]
    FCliente: Proxy<TCliente>;

    [Association([TAssociationProp.Required], [])]
    [JoinColumn('ID_EMPRESA', [TColumnProp.Required])]
    FEmpresa: Proxy<TEmpresa>;

    function GetDescricao: string;
    procedure SetDescricao(const Value: string);
    function GetEndereco: string;
    procedure SetEndereco(const Value: string);
    function GetPorta: Integer;
    procedure SetPorta(const Value: Integer);
    function GetUsuario: string;
    procedure SetUsuario(const Value: string);
    function GetSenha: string;
    procedure SetSenha(const Value: string);
    function GetCliente: TCliente;
    procedure SetCliente(const Value: TCliente);
    function GetEmpresa: TEmpresa;
    procedure SetEmpresa(const Value: TEmpresa);
  public
    [Display('Id', 6, False)]
    property Id: integer read FId;
    [Column('DESCRICAO', [TColumnProp.Required], 100)]
    [Display('Descri��o', 20, True)]
    property Descricao: string read GetDescricao write SetDescricao;
    [Column('ENDERECO', [TColumnProp.Required], 100)]
    [Display('Endere�o', 15, True)]
    property Endereco: string read GetEndereco write SetEndereco;
    [Column('PORTA', [TColumnProp.Required])]
    [Display('Porta', 6, True)]
    property Porta: Integer read GetPorta write SetPorta;
    [Column('USUARIO', [TColumnProp.Required], 100)]
    [Display('Usu�rio', 20, False)]
    property Usuario: string read GetUsuario write SetUsuario;
    [Column('SENHA', [TColumnProp.Required], 100)]
    [Display('Senha', 10, False)]
    property Senha: string read GetSenha write SetSenha;
    [Display('Cliente', 20, True)]
    property Cliente: TCliente read GetCliente write SetCliente;
    [Display('Empresa', 20, True)]
    property Empresa: TEmpresa read GetEmpresa write SetEmpresa;
  end;

  [Entity]
  [Table('EMPRESA')]
  [Sequence('EMPRESA_SEQ')]
  [Id('FID',  TIdGenerator.IdentityOrSequence)]
  TEmpresa = class
  private
    [Column('ID', [TColumnProp.Unique, TColumnProp.Required, TColumnProp.NoUpdate])]
    FID: Integer;
    [Column('NOME', [TColumnProp.Required], 50)]
    FNome: string;
    [ManyValuedAssociation([], CascadeTypeAll, 'FEMPRESA')]
    FUsuarioEmpresa: TList<TUsuarioEmpresa>;
    FMaquina: TList<TMaquina>;
  public
    property ID: Integer read FID write FID;
    property Nome: string read FNome write FNome;
    property UsuarioEmpresa: TList<TUsuarioEmpresa> read FUsuarioEmpresa write FUsuarioEmpresa;
    [ManyValuedAssociation([], CascadeTypeAll, 'FEmpresa')]
    property Maquina: TList<TMaquina> read FMaquina write FMaquina;
    constructor Create;
    destructor Destroy; override;
  end;

  [Entity]
  [Table('CLIENTE')]
  [Sequence('SEQ_CLIENTE')]
  [Id('FId', TIdGenerator.IdentityOrSequence)]
  TCliente = class
  private
    [Column('ID', [TColumnProp.Unique, TColumnProp.Required, TColumnProp.NoUpdate])]
    FId: Integer;
    FNome: string;
    FMaquina: TList<TMaquina>;
    function GetNome: string;
    procedure SetNome(const Value: string);
  public
    [Display('Id', 6, False) ]
    property Id: integer read FId;
    [Column('NOME', [TColumnProp.Required], 100)]
    [Display('Nome', 55) ]
    property Nome: string read GetNome write SetNome;
    [ManyValuedAssociation([], CascadeTypeAll, 'FCliente')]
    property Maquina: TList<TMaquina> read FMaquina write FMaquina;
    constructor Create;
    destructor Destroy; override;
  end;

  [Entity]
  [Table('USUARIO')]
  [Sequence('USUARIO_SEQ')]
  [Id('FID', TIdGenerator.IdentityOrSequence)]
  TUsuario = class
  private
    [Column('ID', [TColumnProp.Unique, TColumnProp.Required, TColumnProp.NoUpdate])]
    FID: Integer;

    [Column('NOME', [TColumnProp.Required], 50)]
    FNome: string;

    [Column('SENHA', [TColumnProp.Required], 20)]
    FSenha: string;

    [ManyValuedAssociation([], CascadeTypeAll, 'FUSUARIO')]
    FUsuarioEmpresa: TList<TUsuarioEmpresa>;
  public
    constructor Create;
    destructor Destroy; override;
    property ID: Integer read FID write FID;
    property Nome: string read FNome write FNome;
    property Senha: string read FSenha write FSenha;
    property UsuarioEmpresa: TList<TUsuarioEmpresa> read FUsuarioEmpresa write FUsuarioEmpresa;
  end;

  [Entity]
  [Table('USUARIO_EMPRESA')]
  [Sequence('SEQ_USUARIO_EMPRESA')]
  [Id('FID',  TIdGenerator.IdentityOrSequence)]
  TUsuarioEmpresa = class
  private
    [Column('ID', [TColumnProp.Unique, TColumnProp.Required, TColumnProp.NoUpdate])]
    FID: Integer;

    [Association([TAssociationProp.Required], [TCascadeType.SaveUpdate, TCascadeType.Merge])]
    [JoinColumn('USUARIO_ID', [TColumnProp.Required], 'ID')]
    FUsuario: Proxy<TUsuario>;

    [Association([TAssociationProp.Required], [TCascadeType.SaveUpdate, TCascadeType.Merge])]
    [JoinColumn('EMPRESA_ID', [TColumnProp.Required], 'ID')]
    FEmpresa: Proxy<TEmpresa>;

    function GetEmpresa: TEmpresa;
    function GetUsuario: TUsuario;
    procedure SetUsuario(const Value: TUsuario);
    procedure SetEmpresa(const Value: TEmpresa);
  public
    property ID: Integer read FID write FID;
    property Usuario: TUsuario read GetUsuario write SetUsuario;
    property Empresa: TEmpresa read GetEmpresa write SetEmpresa;
  end;

implementation

{ TUsuarioEmpresa }

function TUsuarioEmpresa.GetEmpresa: TEmpresa;
begin
  Result := FEmpresa.Value;
end;

function TUsuarioEmpresa.GetUsuario: TUsuario;
begin
  Result := FUsuario.Value;
end;

procedure TUsuarioEmpresa.SetEmpresa(const Value: TEmpresa);
begin
  FEmpresa.Value := Value;
end;

procedure TUsuarioEmpresa.SetUsuario(const Value: TUsuario);
begin
  FUsuario.Value := Value;
end;

{ TUsuario }

constructor TUsuario.Create;
begin
  FUsuarioEmpresa := TList<TUsuarioEmpresa>.Create;
end;

destructor TUsuario.Destroy;
begin
  FUsuarioEmpresa.Free;
  inherited;
end;

{ TCliente }

constructor TCliente.Create;
begin
  FMaquina := TList<TMaquina>.Create;
end;

destructor TCliente.Destroy;
begin
  FMaquina.Free;
  inherited;
end;

function TCliente.GetNome: string;
begin
  Result := FNome;
end;

procedure TCliente.SetNome(const Value: string);
begin
  FNome := Value;
end;

{ TMaquina }

function TMaquina.GetCliente: TCliente;
begin
  Result := FCliente.Value;
end;

function TMaquina.GetDescricao: string;
begin
  Result := FDescricao;
end;

function TMaquina.GetEmpresa: TEmpresa;
begin
  Result := FEmpresa.Value;
end;

function TMaquina.GetEndereco: string;
begin
  Result := FEndereco;
end;

function TMaquina.GetPorta: Integer;
begin
  Result := FPorta;
end;

function TMaquina.GetSenha: string;
begin
  Result := FSenha;
end;

function TMaquina.GetUsuario: string;
begin
  Result := FUsuario;
end;

procedure TMaquina.SetCliente(const Value: TCliente);
begin
  FCliente.Value := Value;
end;

procedure TMaquina.SetDescricao(const Value: string);
begin
  FDescricao := Value;
end;

procedure TMaquina.SetEmpresa(const Value: TEmpresa);
begin
  FEmpresa.Value := Value;
end;

procedure TMaquina.SetEndereco(const Value: string);
begin
  FEndereco := Value;
end;

procedure TMaquina.SetPorta(const Value: Integer);
begin
  FPorta := Value;
end;

procedure TMaquina.SetSenha(const Value: string);
begin
  FSenha := Value;
end;

procedure TMaquina.SetUsuario(const Value: string);
begin
  FUsuario := Value;
end;

{ TEmpresa }

constructor TEmpresa.Create;
begin
  FUsuarioEmpresa := TList<TUsuarioEmpresa>.Create;
  FMaquina := TList<TMaquina>.Create;
end;

destructor TEmpresa.Destroy;
begin
  FUsuarioEmpresa.Free;
  FMaquina.Free;
  inherited;
end;

end.
