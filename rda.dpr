program rda;

uses
  Vcl.Forms,
  Main in 'Main.pas' {FormMain},
  Utils in 'Utils.pas',
  DBConnection in 'DBConnection.pas',
  EditarCliente in 'GUI\EditarCliente.pas' {Form1},
  BaseForm in 'GUI\BaseForm.pas',
  ClienteController in 'Controller\ClienteController.pas',
  EditarMaquina in 'GUI\EditarMaquina.pas' {frEditarMaquina},
  MaquinaController in 'Controller\MaquinaController.pas',
  ConnectionSettings in 'ConnectionSettings.pas' {frConexao},
  Login in 'Login.pas' {frLogin},
  Settings in 'Settings.pas',
  ListarClientes in 'GUI\ListarClientes.pas' {frListarClientes},
  ListarMaquinas in 'GUI\ListarMaquinas.pas' {frListarMaquinas},
  Usuario in 'Entities\Usuario.pas',
  UsuarioController in 'Controller\UsuarioController.pas',
  EditarUsuario in 'GUI\EditarUsuario.pas' {frEditarUsuario},
  ListarUsuarios in 'GUI\ListarUsuarios.pas' {frListarUsuarios},
  Security in 'Security.pas',
  EditarEmpresa in 'GUI\EditarEmpresa.pas' {frEditarEmpresa},
  ListarEmpresas in 'GUI\ListarEmpresas.pas' {frListarEmpresas},
  EmpresaController in 'Controller\EmpresaController.pas',
  DataSetAttribute in 'DataSetAttribute.pas',
  Historico in 'Entities\Historico.pas',
  SqlMonitor in 'SqlMonitor.pas' {FrmSqlMonitor},
  ListarMaquinasConexao in 'GUI\ListarMaquinasConexao.pas' {frmListarMaquinasConexao};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFormMain, FormMain);
  Application.Run;
end.
