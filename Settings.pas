unit Settings;

interface

uses  System.SysUtils;

type
  TSGBD = (dbdSQLite, dbdMSSQL, dbdOracle, dbdFirebid, dbdInterbase, dbdMySQL);
  TAureliusDriver = (adSQLite, adFiredac);

  TSGBDHelper = record helper for TSGBD
    function ToString: string;
    function FireDacDriverID: string;
  end;

  TTAureliusDriverHelper = record helper for TAureliusDriver
    function ToString: string;
  end;

  TSettings = class sealed
  private
    type
      TUsuario = record
        Id: Integer;
        Nome: string;
      end;

      TEmpresa = record
        Id: Integer;
        Nome: string;
      end;

      TDatabase = record
        Driver: TAureliusDriver;
        SGBD: TSGBD;
        Servidor: string;
        Banco: string;
        Usuario: string;
        Senha: string;
      end;
  private
    class var _Instance: TSettings;
  public
    Usuario: TUsuario;
    Empresa: TEmpresa;
    Database: TDatabase;
  public
    destructor Destroy; override;
    class function Instance: TSettings;
    class function NewInstance: TObject; override;
    function ConfigFile: string;
  end;

implementation

{ TSettings }

function TSettings.ConfigFile: string;
begin
  Result := IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0))) + 'Config.ini'
end;

destructor TSettings.Destroy;
begin
  _Instance.Free;
  inherited;
end;

class function TSettings.Instance: TSettings;
begin
  if (_Instance = nil) then
    _Instance := TSettings.Create;
  Result := _Instance;
end;

class function TSettings.NewInstance: TObject;
begin
  if (_Instance = nil) then
    _Instance := inherited NewInstance as Self;
  Result := _Instance;
end;

{ TDBDriverHelper }

function TSGBDHelper.FireDacDriverID: string;
begin
  case Self of
    dbdSQLite: Result := 'SQLite';
    dbdMSSQL: Result := 'MSSQL';
  end;
end;

function TSGBDHelper.ToString: string;
begin
  case Self of
    dbdSQLite: Result := 'SQLite';
    dbdMSSQL: Result := 'SQL Server';
    dbdOracle: Result := 'Oracle';
    dbdFirebid: Result := 'Firebird';
    dbdInterbase: Result := 'Interbase';
    dbdMySQL: Result := 'MySQL';
  else
    Result := 'Desconhecido';
  end;
end;

{ TTAureliusDriverHelper }

function TTAureliusDriverHelper.ToString: string;
begin
  case Self of
    adSQLite: Result := 'SQLite';
    adFiredac: Result := 'MSSQL';
  end;
end;

end.
