unit DBConnection;

interface

uses
  Generics.Collections, Classes, IniFiles, Dialogs,System.UITypes,
  Aurelius.Commands.Listeners,
  Aurelius.Drivers.Interfaces,
  Aurelius.Engine.AbstractManager,
  Aurelius.Engine.ObjectManager,
  Aurelius.Engine.DatabaseManager,
  //Firedac

  Aurelius.Drivers.FireDac,
  FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait,
  FireDAC.Comp.UI, Data.DB, FireDAC.Comp.Client, FireDAC.DApt,
  FireDAC.Phys.MSSQL,
  //SQLite
  Aurelius.Drivers.SQLite,
  Aurelius.Sql.MSSQL,
  Aurelius.Sql.DB2,
  Aurelius.Sql.Firebird,
  Aurelius.Sql.Interbase,
  Aurelius.Sql.MySql,
  Aurelius.Sql.Oracle,
  Aurelius.Sql.PostgreSQL,
  Aurelius.Sql.SQLite;


type
  TDBConnection = class sealed
  private
    class var FInstance: TDBConnection;
  private
    FConnection: IDBConnection;
    FListeners: TList<ICommandExecutionListener>;
    procedure PrivateCreate;
    procedure PrivateDestroy;

    function CreateAdapter: IDBConnection;
    function CreateConnection: IDBConnection;
    function GetConnection: IDBConnection;
    procedure AddListeners(AManager: TAbstractManager);
    function CreateAdapterSQLite: IDBConnection;
    function CreateAdapterFireDac: IDBConnection;
  public
    class function GetInstance: TDBConnection;
    procedure AddCommandListener(Listener: ICommandExecutionListener);
    class procedure AddLines(List: TStrings; SQL: string; Params: TEnumerable<TDBParam>);
    property Connection: IDBConnection read GetConnection;
    function HasConnection: boolean;
    function CreateObjectManager: TObjectManager;
    function GetNewDatabaseManager: TDatabaseManager;
    procedure UnloadConnection;
    function DefaultSQLiteDatabase: string;
    procedure BuildDatabase;
    procedure DestroyDatabase;
  end;

implementation

uses
  Variants, SysUtils, TypInfo, Settings;

{ TConexaoUnica }

procedure TDBConnection.AddCommandListener(
  Listener: ICommandExecutionListener);
begin
  FListeners.Add(Listener);
end;

class procedure TDBConnection.AddLines(List: TStrings; SQL: string;
  Params: TEnumerable<TDBParam>);
var
  P: TDBParam;
  ValueAsString: string;
  HasParams: Boolean;
begin
  List.Add(SQL);

  if Params <> nil then
  begin
    HasParams := False;
    for P in Params do
    begin
      if not HasParams then
      begin
        List.Add('');
        HasParams := True;
      end;

      if P.ParamValue = Variants.Null then
        ValueAsString := 'NULL'
      else
      if P.ParamType = ftDateTime then
        ValueAsString := '"' + DateTimeToStr(P.ParamValue) + '"'
      else
      if P.ParamType = ftDate then
        ValueAsString := '"' + DateToStr(P.ParamValue) + '"'
      else
        ValueAsString := '"' + VarToStr(P.ParamValue) + '"';

      List.Add(P.ParamName + ' = ' + ValueAsString + ' (' +
        GetEnumName(TypeInfo(TFieldType), Ord(P.ParamType)) + ')');
    end;
  end;

  List.Add('');
  List.Add('================================================');
end;

procedure TDBConnection.AddListeners(AManager: TAbstractManager);
var
  Listener: ICommandExecutionListener;
begin
  for Listener in FListeners do
    AManager.AddCommandListener(Listener);
end;


procedure TDBConnection.BuildDatabase;
var
  DatabaseManager: TDatabaseManager;
begin
  DatabaseManager := TDBConnection.GetInstance.GetNewDatabaseManager;
  try
    DatabaseManager.BuildDatabase;
    ShowMessage('Banco de dados construido com sucesso.');
  finally
    DatabaseManager.Free;
  end;
end;

procedure TDBConnection.UnloadConnection;
begin
  if FConnection <> nil then
  begin
    FConnection.Disconnect;
    FConnection := nil;
  end;
end;

function TDBConnection.CreateConnection: IDBConnection;
begin
  if FConnection <> nil then
    Exit(FConnection);

  if FConnection = nil then
    FConnection := CreateAdapter;
  if FConnection = nil then
    Exit;
  Result := FConnection;
end;

function TDBConnection.GetConnection: IDBConnection;
begin
  Result := CreateConnection;
  if Result = nil then
    raise Exception.Create('Invalid connection settings. Cannot connect to database.');
  if not Result.IsConnected then
    Result.Connect;
end;

class function TDBConnection.GetInstance: TDBConnection;
begin
  if FInstance = nil then
  begin
    FInstance := TDBConnection.Create;
    FInstance.PrivateCreate;
  end;
  Result := FInstance;
end;

function TDBConnection.GetNewDatabaseManager: TDatabaseManager;
begin
  Result := TDatabaseManager.Create(Connection);
  AddListeners(Result);
end;

function TDBConnection.CreateObjectManager: TObjectManager;
begin
  Result := TObjectManager.Create(Connection);
  Result.OwnsObjects := True;
  AddListeners(Result);
end;

function TDBConnection.CreateAdapterSQLite: IDBConnection;
begin
  Result := TSQLiteNativeConnectionAdapter.Create(TSettings.Instance.Database.Banco)
end;

function TDBConnection.CreateAdapterFireDac: IDBConnection;
var
  Conn: TFDConnection;
  ConnectionName: string;
begin
  Result := nil;
  Conn := TFDConnection.Create(nil);
  Conn.LoginPrompt := False;
  Conn.ConnectionName := 'RDA';
  Conn.DriverName := TSettings.Instance.Database.SGBD.FireDacDriverID;
  Conn.Params.DriverID := TSettings.Instance.Database.SGBD.FireDacDriverID;
  Conn.Params.Values['User_Name'] := TSettings.Instance.Database.Usuario;
  Conn.Params.Values['Password'] := TSettings.Instance.Database.Senha;
  Conn.Params.Values['Server'] := TSettings.Instance.Database.Servidor;
  Conn.Params.Values['Database'] := TSettings.Instance.Database.Banco;
  Conn.Params.Values['DriverID'] := TSettings.Instance.Database.SGBD.FireDacDriverID;
  Result := TFireDacConnectionAdapter.Create(Conn, True);
end;

function TDBConnection.CreateAdapter: IDBConnection;
var
  SQLiteFile: string;
begin
  Result := nil;
  case TSettings.Instance.Database.Driver of
    adSQLite: Result := CreateAdapterSQLite;
    adFiredac: Result := CreateAdapterFireDac;
  end;
end;

function TDBConnection.DefaultSQLiteDatabase: string;
begin
  Result := IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0))) + 'rda.sqlite';
end;

procedure TDBConnection.DestroyDatabase;
var
  DatabaseManager: TDatabaseManager;
begin
  if MessageDlg('Destruir Banco de Dados?', mtWarning, mbYesNo, 0) = mrYes then
  begin
    DatabaseManager := TDBConnection.GetInstance.GetNewDatabaseManager;
    try
      DatabaseManager.DestroyDatabase;
      ShowMessage('Banco de dados destruido com sucesso.');
    finally
      DatabaseManager.Free;
    end;
  end;
end;

function TDBConnection.HasConnection: boolean;
begin
  Result := CreateConnection <> nil;
end;

procedure TDBConnection.PrivateCreate;
begin
  FListeners := TList<ICommandExecutionListener>.Create;
  CreateConnection;
end;

procedure TDBConnection.PrivateDestroy;
begin
  UnloadConnection;
  FListeners.Free;
end;


initialization

finalization
  if TDBConnection.FInstance <> nil then
  begin
    TDBConnection.FInstance.PrivateDestroy;
    TDBConnection.FInstance.Free;
    TDBConnection.FInstance := nil;
  end;

end.
